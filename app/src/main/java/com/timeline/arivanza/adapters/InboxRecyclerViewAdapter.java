package com.timeline.arivanza.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;
import com.timeline.arivanza.MySingleton;
import com.timeline.arivanza.R;
import com.timeline.arivanza.VideoPlayerActivity;
import com.timeline.arivanza.VimeoVideoActivity;
import com.timeline.arivanza.config.DeveloperKey;
import com.timeline.arivanza.model.InboxItem;
import com.timeline.arivanza.InboxFragment.OnListFragmentInteractionListener;
import com.timeline.arivanza.util.ImageUtil;
import com.timeline.arivanza.util.VideoUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link InboxItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class InboxRecyclerViewAdapter extends RecyclerView.Adapter {

    private static final String TAG = "InboxRVAdapter";
    private final List<InboxItem> mInboxItems;
    private InboxItem mInboxItem;
    private final OnListFragmentInteractionListener mListener;
    private final Context mContext;

    public InboxRecyclerViewAdapter(Context context, List<InboxItem> items, OnListFragmentInteractionListener listener) {
        mInboxItems = items;
        mListener = listener;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        //add layout based on the item type
        switch (InboxItem.Type.values()[viewType]) {
            case ITEM_TYPE_YOUTUBE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_inbox_video_youtube, parent, false);
                return new YouTubeVideoViewHolder(view);
            case ITEM_TYPE_IMAGE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_inbox_image, parent, false);
                return new ImageViewHolder(view);
            case ITEM_TYPE_AUDIO:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_inbox_audio_asset, parent, false);
                return new AudioViewHolder(view);
            case ITEM_TYPE_VIMEO:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_inbox_audio_asset, parent, false);
                return new VimeoVideoHolder(view);
            case ITEM_TYPE_VIDEO:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_inbox_video_asset, parent, false);
                return new VideoViewHolder(view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_inbox_text, parent, false);
                return new TextViewHolder(view);
        }
    }


    // determine which layout to use for the row
    @Override
    public int getItemViewType(int position) {
        InboxItem item = mInboxItems.get(position);
        mInboxItem = item;
        return item.getItemType().ordinal();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        //get inbox item for current position
        final InboxItem item = mInboxItems.get(position);

        //bind values based on the item type
        switch (item.getItemType()) {
            case ITEM_TYPE_YOUTUBE:
                YouTubeVideoViewHolder youTubeVideoViewHolder = (YouTubeVideoViewHolder) holder;

                final YouTubeThumbnailLoader.OnThumbnailLoadedListener  onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener(){
                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                    }

                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                        youTubeThumbnailView.setVisibility(View.VISIBLE);
                    }
                };

                youTubeVideoViewHolder.mYouTubeThumbnailView.initialize(DeveloperKey.DEVELOPER_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(item.getContent());
                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                        //write something for failure
                    }
                });
                break;
            case ITEM_TYPE_VIDEO:
                VideoViewHolder localVideoViewHolder = (VideoViewHolder) holder;
                try {
                    Bitmap bitmap = VideoUtil.getVideoFrameFromVideo(mContext, Uri.fromFile(new File(mInboxItem.getContent())));
                    if(bitmap != null) {
                        bitmap = Bitmap.createScaledBitmap(bitmap, 240, 240, false);
                        localVideoViewHolder.mImageView.setImageBitmap(bitmap);
                    }
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                break;
            case ITEM_TYPE_AUDIO:
                AudioViewHolder audioViewHolder = (AudioViewHolder) holder;
                audioViewHolder.mImageView.setImageResource(R.drawable.ic_mic);
                break;
            case ITEM_TYPE_VIMEO:
                VimeoVideoHolder vimeoVideoHolder = (VimeoVideoHolder) holder;

                //send request to vimeo to obtain thumbnail url
                //https://vimeo.com/api/oembed.json?url=https://vimeo.com/76979871
                String url = "https://vimeo.com/api/oembed.json?url=https://vimeo.com/"+mInboxItem.getContent();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.i(TAG, "resp: "+ response.toString());
                                try {
                                    Picasso.get().load(response.getString("thumbnail_url")).into(vimeoVideoHolder.mImageView);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: Handle error

                            }
                        });

                // Access the RequestQueue through your singleton class.
                MySingleton.getInstance(mContext.getApplicationContext()).addToRequestQueue(jsonObjectRequest);

                break;
            case ITEM_TYPE_IMAGE:
                ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
                try {
                    Log.i(TAG, "Content: " + mInboxItem.getContent());
                    imageViewHolder.mImageView.setImageBitmap(ImageUtil.handleSamplingAndRotationBitmap(mContext, Uri.fromFile(new File(mInboxItem.getContent()))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                TextViewHolder textViewHolder = (TextViewHolder) holder;
                textViewHolder.mContentView.setText(item.getContent());
        }
    }

    @Override
    public int getItemCount() {
        return mInboxItems.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;

        public TextViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final ImageView mImageView;
        public final ImageButton mPlayButton;
        public final String mVideoPath;

        public VideoViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.assetThumbnail);
            mPlayButton = view.findViewById(R.id.buttonAssetPlay);
            mVideoPath = mInboxItem.getContent();

            mPlayButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //start the video activity
            Intent myIntent = new Intent(mContext, VideoPlayerActivity.class);
            myIntent.putExtra("videoPath", mVideoPath);
            mContext.startActivity(myIntent);
        }

    }

    public class AudioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final ImageView mImageView;
        public final ImageButton mPlayButton;
        //creating media player
        final MediaPlayer mediaPlayer = new MediaPlayer();

        public AudioViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.assetThumbnail);
            mPlayButton = view.findViewById(R.id.buttonAssetPlay);

            //init player
            try {
                mediaPlayer.setDataSource(mInboxItem.getContent());
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mPlayButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            } else {
                mediaPlayer.start();
            }
        }

    }

    public class VimeoVideoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final ImageView mImageView;
        public final ImageButton mPlayButton;
        public final String videoId;

        public VimeoVideoHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.assetThumbnail);
            mPlayButton = view.findViewById(R.id.buttonAssetPlay);

            //init player
            videoId = mInboxItem.getContent();

            mPlayButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //start the video activity
            Intent myIntent = new Intent(mContext, VimeoVideoActivity.class);
            myIntent.putExtra("videoId", videoId);
            mContext.startActivity(myIntent);
        }

    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;

        public ImageViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.image);
        }
    }

    public class YouTubeVideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final String TAG = YouTubeVideoViewHolder.class.getName();
        public final View mView;
        public final YouTubeThumbnailView mYouTubeThumbnailView;
        public final ImageButton mPlayButton;

        public YouTubeVideoViewHolder(View view) {
            super(view);
            mView = view;

            mYouTubeThumbnailView = view.findViewById(R.id.youTubeThumbnail);
            mPlayButton = view.findViewById(R.id.buttonYouTubePlay);
            mPlayButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) mContext,
                    DeveloperKey.DEVELOPER_KEY,
                    mInboxItem.getContent(),
                    100,     //after this time, video will start automatically
                    true,               //autoplay or not
                    false);             //lightbox mode or not; show the video in a small box
            mContext.startActivity(intent);
        }
    }
}
