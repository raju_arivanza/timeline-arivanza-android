package com.timeline.arivanza.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoUtil {
    // regular expressions
    private static final Pattern YOUTUBE_PATTERN = Pattern.compile(".*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?v=)([^#\\&\\?]*).*", Pattern.CASE_INSENSITIVE);
    private static final Pattern VIMEO_PATTERN = Pattern.compile("[http|https]+:\\/\\/(?:www\\.|)vimeo\\.com\\/([a-zA-Z0-9_\\-]+)(&.+)?", Pattern.CASE_INSENSITIVE);

    // youtube thumbnail settings
    private static final String YOUTUBE_THUMBNAIL_URL = "http://i.ytimg.com/vi/";
    private static final String YOUTUBE_THUMBNAIL_SIZE = "/hqdefault.jpg";

    public static boolean isYouTubeUrl(String url) {
        return YOUTUBE_PATTERN.matcher(url).matches();
    }

    public static boolean isVimeoUrl(String url) {
        return VIMEO_PATTERN.matcher(url).matches();
    }

    public static String getYouTubeThumbnailUrl(@NonNull String url) {
        Matcher youtube = YOUTUBE_PATTERN.matcher(url);
        if (!youtube.find())
            return null;

        String group = youtube.group(1);

        return YOUTUBE_THUMBNAIL_URL + group + YOUTUBE_THUMBNAIL_SIZE;
    }

    public static String getVimeoVideoId(@NonNull String url) {
        Matcher vimeo = VIMEO_PATTERN.matcher(url);
        if (!vimeo.find())
            return null;

        return vimeo.group(1);
    }

    public static String getYoutubeVideoId(@NonNull String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url); //url is youtube url for which you want to extract the id.
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    public static Bitmap getVideoFrameFromVideo(Context context, Uri videoUri)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(context, videoUri);

            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in getVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
}
