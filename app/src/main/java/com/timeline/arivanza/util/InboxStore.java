package com.timeline.arivanza.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.timeline.arivanza.R;
import com.timeline.arivanza.model.InboxItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InboxStore {

    private void save(Context context, List<InboxItem> items) {
        SharedPreferences.Editor editor = context.getSharedPreferences(context.getString(R.string.inbox_key),Context.MODE_PRIVATE).edit();
        String json = new Gson().toJson(items);
        editor.putString(context.getString(R.string.inbox_items), json);
        editor.apply();
    }

    public void add(Context context, InboxItem item) {
        List<InboxItem> items = getAll(context);
        if (items == null) {
            items = new ArrayList<>();
        }
        items.add(item);
        save(context, items);
    }
    public void add(Context context, List<InboxItem> newItems) {
        List<InboxItem> items = getAll(context);
        if (items == null) {
            items = new ArrayList<>();
        }
        items.addAll(newItems);
        save(context, items);
    }

    public List<InboxItem> getAll(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.inbox_key), Context.MODE_PRIVATE);

        if (sharedPreferences.contains(context.getString(R.string.inbox_items))) {
            String jsonInbox = sharedPreferences.getString(context.getString(R.string.inbox_items), null);
            InboxItem[] items = new Gson().fromJson(jsonInbox, InboxItem[].class);
            return new ArrayList<InboxItem>(Arrays.asList(items));
        }
        return new ArrayList<>();

    }
}
