package com.timeline.arivanza

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.timeline.arivanza.model.InboxItem
import com.timeline.arivanza.util.InboxStore

import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerFragment
import com.timeline.arivanza.config.DeveloperKey
import android.widget.Toast



class MainActivity : AppCompatActivity(), InboxFragment.OnListFragmentInteractionListener {

    private val RECOVERY_DIALOG_REQUEST = 1

    private val TAG = "MainActivity"


    override fun onListFragmentInteraction(item: InboxItem?) {
        Snackbar.make(findViewById(R.id.fragment), item?.itemType!!.name, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

    private val mInboxStore = InboxStore()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar);
        toolbar_title.text = resources.getText(R.string.inbox)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.menu_main, menu)
//        menuInflater.inflate(R.menu.menu_student, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
