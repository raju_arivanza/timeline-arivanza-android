package com.timeline.arivanza;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.timeline.arivanza.model.InboxItem;
import com.timeline.arivanza.util.AndroidPermissions;
import com.timeline.arivanza.util.ImageUtil;
import com.timeline.arivanza.util.RealPathUtil;
import com.timeline.arivanza.util.InboxStore;
import com.timeline.arivanza.util.VideoUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Activity to handle content shared from ecosystem
 */
public class ShareActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{

    private static final int REQUEST_STORAGE_FOR_SINGLE_IMAGE = 1001;
    private static final int REQUEST_STORAGE_FOR_MULTIPLE_IMAGE = 1002;
    InboxStore mInboxStore = new InboxStore();
    InboxItem mInboxItem;
    List<InboxItem> mInboxItems;
    ConstraintLayout mDialogContainer;
    ImageView mPostedImage;
    Uri mImageUri;
//    private static final String FILE_PROTOCOL = "file:/";
    private static final String TAG = ShareActivity.class.getName();

    public static final String TYPE_AUDIO = "audio";
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_APP = "application";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        //set dimensions as this is a floating window
        mDialogContainer = findViewById(R.id.activity_container);
        Log.i(TAG, "Resources.getSystem().getDisplayMetrics().widthPixels : "+ Resources.getSystem().getDisplayMetrics().widthPixels);
        mDialogContainer.getLayoutParams().width = Resources.getSystem().getDisplayMetrics().widthPixels;
        mDialogContainer.getLayoutParams().height = Resources.getSystem().getDisplayMetrics().heightPixels;
//        ConstraintLayout constraintLayout = findViewById(R.id.constraint_layout);
//        constraintLayout.setClipToOutline(true);
//        dialogContainer.requestLayout();

        //add listeners for cancel and inbox buttons
        Button btnCancel = findViewById(R.id.button_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //animate window before closing
                takeWindowOutFocus();
                //close activity
                mDialogContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 500);
            }
        });
        Button btnSendToInbox = findViewById(R.id.button_send_to_inbox);
        btnSendToInbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //save to shared preferences
                if(mInboxItems != null && mInboxItems.size() > 0) {
                    mInboxStore.add(ShareActivity.this, mInboxItems);
                } else if(mInboxItem.getItemType().equals(InboxItem.Type.ITEM_TYPE_WEB_PAGE)){
                    //process webpage pull in background and save prefs
                    new LoadWebSiteContent().execute(mInboxItem.getContent());
                } else {
                    mInboxStore.add(ShareActivity.this, mInboxItem);
                }
                //change view to confirm inbox success
                showSuccess();

                //close activity
                mDialogContainer.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 1500);
            }
        });

        //member view components
        mPostedImage = findViewById(R.id.posted_image);
        //get reference for content type icon & content text
        ImageView contentTypeIcon = findViewById(R.id.icon_content_type);
        TextView contentText = findViewById(R.id.content_text);

        //disable close on touch outside
        this.setFinishOnTouchOutside(false);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            Log.i(TAG, "MIME...... "+ type);
            if (type.startsWith(TYPE_TEXT)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                //check if url
                if(URLUtil.isValidUrl(sharedText)) {
                    //check url for youtube or vimeo
                    if(VideoUtil.isYouTubeUrl(sharedText) || VideoUtil.isVimeoUrl(sharedText)) {
                        contentTypeIcon.setImageResource(R.drawable.ic_video);
                        contentText.setText(R.string.share_content_video);
                        if(VideoUtil.isYouTubeUrl(sharedText)) {
                            mInboxItem = new InboxItem(InboxItem.Type.ITEM_TYPE_YOUTUBE, VideoUtil.getYoutubeVideoId(sharedText));
                        } else {
                            mInboxItem = new InboxItem(InboxItem.Type.ITEM_TYPE_VIMEO, VideoUtil.getVimeoVideoId(sharedText));
                        }
                    } else {
                        //just a plain url
                        contentTypeIcon.setImageResource(R.drawable.ic_web_page);
                        contentText.setText(R.string.share_content_web_page);
                        mInboxItem = new InboxItem(InboxItem.Type.ITEM_TYPE_WEB_PAGE, sharedText);
                    }
                } else {
                    contentText.setText(sharedText);
                    mInboxItem = new InboxItem(InboxItem.Type.ITEM_TYPE_TEXT, sharedText);
                    contentText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    contentText.setEllipsize(TextUtils.TruncateAt.END);
                    contentText.setMaxLines(15);
                    ConstraintSet cs = new ConstraintSet();
                    cs.clone(mDialogContainer);
                    cs.constrainPercentWidth(R.id.content_text, .66f);
                    cs.applyTo(mDialogContainer);

                    //cleanup unwanted
                    findViewById(R.id.space_content_icon_top).setVisibility(View.GONE);
                    findViewById(R.id.space_content_icon_bottom).setVisibility(View.GONE);
                    findViewById(R.id.space_down_arrow_bottom).setVisibility(View.GONE);
                    findViewById(R.id.icon_down_arrow).setVisibility(View.GONE);
                    findViewById(R.id.icon_content_type).setVisibility(View.GONE);
                }

            } else if (type.startsWith(TYPE_IMAGE)) {
                mImageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);

                //request android permissions
                // Verify that all required disk permissions have been granted.
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Contacts permissions have not been granted.
                    Log.i(TAG, "Storage permissions has NOT been granted. Requesting permissions.");
                    requestStoragePermissions(action);
                } else {
                    //granted already
                    Log.i(TAG,
                            "Storage permissions have already been granted. Displaying image.");
                    showProcessedImage();
                }
//                contentTypeIcon.setImageURI(uri);
                handleMediaContent(intent, InboxItem.Type.ITEM_TYPE_IMAGE); // Handle media being sent
                //hide other parts
                contentTypeIcon.setVisibility(View.GONE);
                findViewById(R.id.space_content_icon_top).setVisibility(View.GONE);
                findViewById(R.id.space_content_icon_bottom).setVisibility(View.GONE);
                findViewById(R.id.space_down_arrow_bottom).setVisibility(View.GONE);
                findViewById(R.id.icon_down_arrow).setVisibility(View.GONE);
                findViewById(R.id.content_text).setVisibility(View.GONE);
            } else if (type.startsWith(TYPE_AUDIO)) {
                contentTypeIcon.setImageResource(R.drawable.ic_mic);
                contentText.setText(R.string.share_content_audio);
                handleMediaContent(intent, InboxItem.Type.ITEM_TYPE_AUDIO); // Handle media being sent
            } else if (type.startsWith(TYPE_VIDEO)) {
                contentTypeIcon.setImageResource(R.drawable.ic_video);
                contentText.setText(R.string.share_content_video);
                handleMediaContent(intent, InboxItem.Type.ITEM_TYPE_VIDEO); // Handle media being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith(TYPE_IMAGE)) {

                //request android permissions
                // Verify that all required disk permissions have been granted.
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    // Contacts permissions have not been granted.
                    Log.i(TAG, "Storage permissions has NOT been granted. Requesting permissions.");
                    requestStoragePermissions(action);
                } else {
                    //granted already
                    Log.i(TAG,
                            "Storage permissions have already been granted. Displaying image.");
                }

                contentTypeIcon.setImageResource(R.drawable.ic_image_stack);
                contentText.setText(R.string.share_content_image_stack);
                handleMultipleImages(intent); // Handle multiple images being sent
            }
        }

        //animate window incoming
        mDialogContainer.postDelayed(new Runnable() {
            @Override
            public void run() {
                bringWindowInFocus();
            }
        }, 500);
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_STORAGE_FOR_MULTIPLE_IMAGE || requestCode == REQUEST_STORAGE_FOR_SINGLE_IMAGE) {
            Log.i(TAG, "Received response for Storage permissions request.");

            // We have requested multiple permissions for Storage, so all of them need to be
            // checked.
            if (AndroidPermissions.verifyPermissions(grantResults)) {
                // All required permissions have been granted, display image preview.
                Snackbar.make(mDialogContainer, R.string.permision_available_storage,
                        Snackbar.LENGTH_SHORT)
                        .show();

                if(requestCode == REQUEST_STORAGE_FOR_SINGLE_IMAGE) {
                    showProcessedImage();
                }
//                mDialogContainer.requestLayout();
            } else {
                Log.i(TAG, "Storage permissions were NOT granted.");
                Snackbar.make(mDialogContainer, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Requests the Storage permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     * @param action
     */
    private void requestStoragePermissions(String action) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            Log.i(TAG,
                    "Displaying storage permission rationale to provide additional context.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(mDialogContainer, R.string.permission_storage_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat
                                    .requestPermissions(ShareActivity.this, new String[] {
                                            Manifest.permission.READ_EXTERNAL_STORAGE,
                                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                                            },
                                            Intent.ACTION_SEND_MULTIPLE.equals(action) ? REQUEST_STORAGE_FOR_MULTIPLE_IMAGE : REQUEST_STORAGE_FOR_SINGLE_IMAGE);
                        }
                    })
                    .show();
        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    Intent.ACTION_SEND_MULTIPLE.equals(action) ? REQUEST_STORAGE_FOR_MULTIPLE_IMAGE : REQUEST_STORAGE_FOR_SINGLE_IMAGE);
        }
    }

    private void showProcessedImage() {
        try {
            mPostedImage.setImageBitmap(ImageUtil.handleSamplingAndRotationBitmap(this, mImageUri));
        } catch (IOException e) {
            e.printStackTrace();
            mPostedImage.setImageURI(mImageUri);
        }
        mPostedImage.setVisibility(View.VISIBLE);
    }

    private void bringWindowInFocus() {
        ConstraintSet cs1 = new ConstraintSet();
        cs1.clone(mDialogContainer);
        cs1.connect(R.id.aza_brand_container, ConstraintSet.TOP, R.id.top_margin, ConstraintSet.BOTTOM);
        cs1.connect(R.id.button_cancel, ConstraintSet.BOTTOM, R.id.bottom_margin, ConstraintSet.TOP, 0);
        cs1.clear(R.id.button_cancel, ConstraintSet.TOP);
        cs1.connect(R.id.button_send_to_inbox, ConstraintSet.BOTTOM, R.id.bottom_margin, ConstraintSet.TOP, 0);
        cs1.clear(R.id.button_send_to_inbox, ConstraintSet.TOP);

        Transition transition = new ChangeBounds();
        transition.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        transition.setDuration(700);

        TransitionManager.beginDelayedTransition(mDialogContainer, transition);
        cs1.applyTo(mDialogContainer);
    }

    private void takeWindowOutFocus() {
        ConstraintSet cs1 = new ConstraintSet();
        cs1.clone(mDialogContainer);
        cs1.connect(R.id.aza_brand_container, ConstraintSet.TOP, R.id.bottom_margin, ConstraintSet.BOTTOM);
        cs1.connect(R.id.button_cancel, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
        cs1.clear(R.id.button_cancel, ConstraintSet.BOTTOM);
        cs1.connect(R.id.button_send_to_inbox, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
        cs1.clear(R.id.button_send_to_inbox, ConstraintSet.BOTTOM);

        Transition transition = new ChangeBounds();
        transition.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        transition.setDuration(1000);

        TransitionManager.beginDelayedTransition(mDialogContainer, transition);
        cs1.applyTo(mDialogContainer);
    }


    private void showSuccess() {
        ConstraintSet cs1 = new ConstraintSet();
        cs1.clone(mDialogContainer);

        cs1.setVisibility(R.id.icon_down_arrow, ConstraintSet.GONE);
        cs1.setVisibility(R.id.icon_content_type, ConstraintSet.GONE);
        cs1.setVisibility(R.id.content_text, ConstraintSet.GONE);
        cs1.setVisibility(R.id.posted_image, ConstraintSet.GONE);

        cs1.setVisibility(R.id.success_text, ConstraintSet.VISIBLE);
        cs1.setVisibility(R.id.post_success, ConstraintSet.VISIBLE);

        Transition transition = new Slide();
        transition.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        transition.setDuration(500);

        TransitionManager.beginDelayedTransition(mDialogContainer, transition);
        cs1.applyTo(mDialogContainer);
    }

    /**
     * generic to create an inbox item
     * @param intent
     * @param type
     */
    private void handleMediaContent(Intent intent, InboxItem.Type type) {
        Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        String filePath = RealPathUtil.getPath(this, uri);
        mInboxItem = new InboxItem(type, filePath);
    }

    /**
     * handles adding multiple images to inbox
     * @param intent
     */
    void handleMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        mInboxItems = new ArrayList<>();
        String filePath = "";
        if (imageUris != null) {
            for(Uri uri: imageUris) {
                filePath = RealPathUtil.getPath(this, uri);
                mInboxItems.add(new InboxItem(InboxItem.Type.ITEM_TYPE_IMAGE, filePath));
            }
        }
    }

    private class LoadWebSiteContent extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... urls) {

            for(String url: urls) {
                try {
                    Document document = Jsoup.connect(url).get();
                    mInboxItem.setContent(document.text());
                    Log.i(TAG, mInboxItem.getContent());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //save web content
            mInboxStore.add(ShareActivity.this, mInboxItem);
        }
    }
}
