package com.timeline.arivanza.model;

public class InboxItem {

    public InboxItem(Type itemType, String content) {
        this.itemType = itemType;
        this.content = content;
    }

    private Type itemType;
    private String content;

    public Type getItemType() {
        return itemType;
    }

    public void setItemType(Type itemType) {
        this.itemType = itemType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public enum Type {
        ITEM_TYPE_AUDIO, ITEM_TYPE_VIDEO, ITEM_TYPE_YOUTUBE, ITEM_TYPE_VIMEO, ITEM_TYPE_WEB_PAGE, ITEM_TYPE_TEXT, ITEM_TYPE_IMAGE
    }
}
